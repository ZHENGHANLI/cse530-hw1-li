package hw1;

import java.nio.ByteBuffer;
import java.sql.Types;

import java.util.HashMap;
import java.util.*;

/**
 * This class represents a tuple that will contain a single row's worth of information
 * from a table. It also includes information about where it is stored
 * @author Sam Madden modified by Doug Shook
 *
 */
public class Tuple {
	TupleDesc tuple;
	int pid;
	int id;
	HashMap<String, byte[]> map;
	/**
	 * Creates a new tuple with the given description
	 * @param t the schema for this tuple
	 */
	public Tuple(TupleDesc t) {
		map = new HashMap<String,byte[]>();
		tuple = t;
		//your code here
		
	}
	
	public TupleDesc getDesc() {
		//your code here
		return tuple;
	}
	
	/**
	 * retrieves the page id where this tuple is stored
	 * @return the page id of this tuple
	 */
	public int getPid() {
		//your code here
		return pid;
	}

	public void setPid(int pid) {
		//your code here
		this.pid=pid;
	}

	/**
	 * retrieves the tuple (slot) id of this tuple
	 * @return the slot where this tuple is stored
	 */
	public int getId() {
		//your code here
		return id;
	}

	public void setId(int id) {
		//your code here
		this.id=id;
	}
	
	public void setDesc(TupleDesc td) {
		//your code here;
		tuple=td;
	}
	
	/**
	 * Stores the given data at the i-th field
	 * @param i the field number to store the data
	 * @param v the data
	 */
	public void setField(int i, byte[] v) {
		//your code here
		map.put(this.tuple.getFieldName(i), v);
	}
	
	public byte[] getField(int i) {
		//your code here
		return map.get(tuple.getFieldName(i));
	}
	
	/**
	 * Creates a string representation of this tuple that displays its contents.
	 * You should convert the binary data into a readable format (i.e. display the ints in base-10 and convert
	 * the String columns to readable text).
	 */
	public String toString() {
		//your code here
//		String s = "";
//		
//		for(int i = 0; i < map.size(); i++) {
//			if (tuple.getType(i) == Type.INT) {
//				//final ByteBuffer bb = ByteBuffer.wrap(map.get(i));
//				//s+= (bb.getInt()+"\t");
//				s+=map.get(i).toString();
////			}else {
////				byte[] field = map.get(i);
////			
////				int len = field[0];
////
////				char[] s1 = new char[len];
////				for(int j = 1; j < len; j++) {
////					s1[j] = (char)field[j];
////				}
////				String s2 = new String(s1);
////				s += s2 + "\t";
//			}
//		}
//		return s;
		String result = "";
		
		for(int i = 0; i < map.size(); i++)
		{
			if(tuple.getType(i) == Type.INT) {
				//result += java.nio.ByteBuffer.wrap(map.get(i)).getInt() + "\t";
				result += map.get(i).toString();
			} else {
//				Field field = map.get(i);
//				int len = field[0];
//				char[] s = new char[len];
//				for(int j = 1; j < len; i++) {
//					s[j] = (char)field[j];
//				}
//				String s2 = field.toString();
//				result += s2 + "\t";
			}
		}
		return result;
	}
}