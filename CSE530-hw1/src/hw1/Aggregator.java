package hw1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * A class to perform various aggregations, by accepting one tuple at a time
 * @author Doug Shook
 *
 */
public class Aggregator {
	AggregateOperator o;
	boolean groupBy;
	TupleDesc td;
	ArrayList<Tuple> list;
	ArrayList<Tuple> resultList;
	public Aggregator(AggregateOperator o, boolean groupBy, TupleDesc td) {
		//your code here
		this.o=o;
		this.groupBy=groupBy;
		this.td=td;
		list = new ArrayList<Tuple>();
		resultList = new ArrayList<Tuple>();
	}

	/**
	 * Merges the given tuple into the current aggregation
	 * @param t the tuple to be aggregated
	 */
	public void merge(Tuple t) {
		//your code here
		list.add(t);
		if(groupBy) {
			HashSet<Field> h = new HashSet<Field>();
			for(int i=0;i<list.size();i++) {
				h.add(list.get(i).getField(0));
			}
			ArrayList<Field> groups = new ArrayList<Field>();
			groups.addAll(h);
			HashMap<Field,ArrayList<Field>> map=new HashMap<Field,ArrayList<Field>>();
			for(int j= 0;j<groups.size();j++) {
				ArrayList<Field> temp = new ArrayList<Field>();
				for(int i=0;i<list.size();i++) {
					if(list.get(i).getField(0).compare(RelationalOperator.EQ, groups.get(j))) {
						temp.add(list.get(i).getField(1));
					}
				}
				map.put(groups.get(j), temp);
			}
			resultList.clear();
			for(int i=0;i<map.size();i++) {
				Tuple result = new Tuple(td);
				result.setField(0, groups.get(i));
				result.setField(1, single(map.get(groups.get(i))));
				resultList.add(result);
			}
		}
		//Not Group By
		else {
			ArrayList<Field> arl= new ArrayList<Field>();
			for(int i=0;i<list.size();i++) {
				arl.add(list.get(i).getField(0));
			}
			Tuple result = new Tuple(td);
			result.setField(0, single(arl));
			resultList.clear();
			resultList.add(result);
		}
		
	}
	private Field single(ArrayList<Field> arl) {
		//Int Field
		if(arl.get(0).getType()==Type.INT) {
			if(o==AggregateOperator.AVG) {
				int sum=0;
				for(int i=0;i<arl.size();i++) {
					sum+=((IntField)(arl.get(i))).getValue();
				}
				int avg=sum/arl.size();
				return new IntField(avg);
			}
			else if(o==AggregateOperator.SUM) {
				int sum=0;
				for(int i=0;i<arl.size();i++) {
					sum+=((IntField)(arl.get(i))).getValue();
				}
				return new IntField(sum);
			}
			else if(o==AggregateOperator.COUNT) {
				return new IntField(arl.size());
			}
			else if(o==AggregateOperator.MAX) {
				IntField max = (IntField)arl.get(0);
				for(int i=1;i<arl.size();i++) {
					System.out.println(i);
					System.out.println(arl);
					if(arl.get(i).compare(RelationalOperator.GT, max)) {
						max=(IntField)arl.get(i);
					}
				}
				return max;
			}
			else if(o==AggregateOperator.MIN) {
				IntField min = (IntField)arl.get(0);
				for(int i=0;i<arl.size();i++) {
					if(arl.get(i).compare(RelationalOperator.LT, min)) {
						min=(IntField)arl.get(i);
					}
				}
				return min;
			}
		}
		//String Field
		else {
			if((o==AggregateOperator.SUM)||(o==AggregateOperator.AVG)){
				return new StringField("0");
			}
			else if(o==AggregateOperator.COUNT) {
				String s=list.size()+"";
				return new StringField(s);
			}
			else if(o==AggregateOperator.MAX) {
				StringField max = (StringField)arl.get(0);
				for(int i=0;i<arl.size();i++) {
					if(arl.get(i).compare(RelationalOperator.GT, max)) {
						max=(StringField)arl.get(i);
					}
				}
				return max;
			}
			else if(o==AggregateOperator.MIN) {
				StringField min = (StringField)arl.get(0);
				for(int i=0;i<arl.size();i++) {
					if(arl.get(i).compare(RelationalOperator.LT, min)) {
						min=(StringField)arl.get(i);
					}
				}
				return min;
			}
			
		}
		return null;
	}
	/**
	 * Returns the result of the aggregation
	 * @return a list containing the tuples after aggregation
	 */
	public ArrayList<Tuple> getResults() {
		//your code here
		return resultList;
	}

}
