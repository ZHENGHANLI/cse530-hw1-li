package hw1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Iterator;

import hw3.BPlusTree;

/**
 * A heap file stores a collection of tuples. It is also responsible for managing pages.
 * It needs to be able to manage page creation as well as correctly manipulating pages
 * when tuples are added or deleted.
 * @author Sam Madden modified by Doug Shook
 *
 */
public class HeapFile {
	
	public static final int PAGE_SIZE = 4096;
	private File f;
	private TupleDesc type;
	private BPlusTree btree;

	public BPlusTree readBPlusTree() {
		
		return btree;
	}

	
	/**
	 * Creates a new heap file in the given location that can accept tuples of the given type
	 * @param f location of the heap file
	 * @param types type of tuples contained in the file
	 */
	public HeapFile(File f, TupleDesc type) {
		//your code here
		this.f=f;
		this.type=type;
		this.btree=new BPlusTree(PAGE_SIZE/type.getSize());
	}
	
	public BPlusTree getBPlusTree() {
		return btree;
	}
	
	public File getFile() {
		//your code here
		return f;
	}
	
	public TupleDesc getTupleDesc() {
		//your code here
		return type;
	}
	
	/**
	 * Creates a HeapPage object representing the page at the given page number.
	 * Because it will be necessary to arbitrarily move around the file, a RandomAccessFile object
	 * should be used here.
	 * @param id the page number to be retrieved
	 * @return a HeapPage at the given page number
	 */
	public HeapPage readPage(int id) {
		//your code here
		try {
			RandomAccessFile raf = new RandomAccessFile(f, "r");
			byte[] pageData = new byte[HeapFile.PAGE_SIZE];
			int offset = HeapFile.PAGE_SIZE * id;
			raf.seek(offset);
			raf.readFully(pageData);
			raf.close();
			return new HeapPage(id, pageData, this.getId());
		}
		catch(FileNotFoundException e){
			throw new IllegalArgumentException();
		}catch(IOException e) {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Returns a unique id number for this heap file. Consider using
	 * the hash of the File itself.
	 * @return
	 */
	public int getId() {
		//your code here
		return f.getAbsolutePath().hashCode();
	}
	
	/**
	 * Writes the given HeapPage to disk. Because of the need to seek through the file,
	 * a RandomAccessFile object should be used in this method.
	 * @param p the page to write to disk
	 */
	public void writePage(HeapPage p) {
		//your code here
		try {
			RandomAccessFile raf = new RandomAccessFile(f, "rw");
			int offset = HeapFile.PAGE_SIZE * p.getId();
			raf.seek(offset);
			raf.write(p.getPageData(), 0, HeapFile.PAGE_SIZE);
			raf.close();
		}
		catch(FileNotFoundException e){
			throw new IllegalArgumentException();
		}catch(IOException e) {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Adds a tuple. This method must first find a page with an open slot, creating a new page
	 * if all others are full. It then passes the tuple to this page to be stored. It then writes
	 * the page to disk (see writePage)
	 * @param t The tuple to be stored
	 * @return The HeapPage that contains the tuple
	 */
	public HeapPage addTuple(Tuple t) throws Exception{
		//your code here
		ArrayList<HeapPage> openPages= new ArrayList<HeapPage>();
		for (int i=0;i<this.getNumPages();i++) {
			HeapPage page = this.readPage(i);
			boolean hasEmptySlots = false;
			for (int j = 0; j< page.getNumSlots(); j++) {
				if (!page.slotOccupied(i)) {
					hasEmptySlots=true;
					break;
				}
			}
			if(hasEmptySlots) {
				openPages.add(page);
			}
		}
		HeapPage p;
		if(openPages.isEmpty()) {
			p=new HeapPage(this.getNumPages()+1,new byte[HeapFile.PAGE_SIZE],this.getId());
			p.addTuple(t);
		}
		else {
			p = openPages.get(0);
			p.addTuple(t);
		}
		writePage(p);
		return p;
	}
	
	/**
	 * This method will examine the tuple to find out where it is stored, then delete it
	 * from the proper HeapPage. It then writes the modified page to disk.
	 * @param t the Tuple to be deleted
	 */
	public void deleteTuple(Tuple t)throws Exception{
		//your code here
		int pid=t.getPid();
		HeapPage p=this.readPage(pid);
		p.deleteTuple(t);
		writePage(p);
	}
	
	/**
	 * Returns an ArrayList containing all of the tuples in this HeapFile. It must
	 * access each HeapPage to do this (see iterator() in HeapPage)
	 * @return
	 */
	public ArrayList<Tuple> getAllTuples() {
		//your code here
		ArrayList<Tuple> result = new ArrayList<Tuple>();
		for (int i=0;i<this.getNumPages();i++) {
			HeapPage page = this.readPage(i);
			Iterator<Tuple> iter = page.iterator();
			while(iter.hasNext()) {
				result.add(iter.next());
			}
		}
		return result;
	}
	
	/**
	 * Computes and returns the total number of pages contained in this HeapFile
	 * @return the number of pages
	 */
	public int getNumPages() {
		//your code here
		return (int)Math.ceil(f.length()/HeapFile.PAGE_SIZE);
	}
	
	
	
	
	
}