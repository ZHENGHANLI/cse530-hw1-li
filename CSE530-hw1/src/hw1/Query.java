package hw1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.parser.*;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.*;
import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.FromItemVisitor;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.LateralSubSelect;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.statement.select.SubJoin;
import net.sf.jsqlparser.statement.select.SubSelect;
import net.sf.jsqlparser.statement.select.TableFunction;
import net.sf.jsqlparser.statement.select.ValuesList;

public class Query {

	private String q;
	private Catalog c;
	public Query(String q) {
		this.q = q;
	}
	
	public Relation execute()  {
		Statement statement = null;
		try {
			statement = CCJSqlParserUtil.parse(q);
		} catch (JSQLParserException e) {
			System.out.println("Unable to parse query");
			e.printStackTrace();
		}
		Select selectStatement = (Select) statement;
		PlainSelect sb = (PlainSelect)selectStatement.getSelectBody();
		//Get Catalog
		c=Database.getCatalog();
		
		//Get Select Items
		ArrayList<ColumnVisitor> sList=new ArrayList<ColumnVisitor>();
		List<SelectItem> sItem=sb.getSelectItems();
		for(int i=0;i<sItem.size();i++) {
			ColumnVisitor cv= new ColumnVisitor();
			sItem.get(i).accept(cv);
			sList.add(cv);
		}
		
		
		//Get From
		FromItem fItem=sb.getFromItem();
		Table fromTable=(Table)fItem;

		//Get Where Expression
		WhereExpressionVisitor wv=new WhereExpressionVisitor();
		if(sb.toString().contains("WHERE")) {
			Expression where = sb.getWhere();
			where.accept(wv);
		}
		
		
		//Get Join
		ArrayList<FromItem> joinRight= new ArrayList<FromItem>();
		ArrayList<String> joinOn=new ArrayList<String>();
		if(sb.toString().contains("JOIN")) {
			List<Join> joins=sb.getJoins();
			for(int i=0;i<joins.size();i++) {
				joinRight.add(joins.get(i).getRightItem());
				joinOn.add(joins.get(i).getOnExpression().toString());
			}
		}
		
		
		
		//Get Group By
		ArrayList<String> groupby=new ArrayList<String>();
		if(sb.toString().contains("GROUP BY")){
			List<Expression> gb = sb.getGroupByColumnReferences();
			for(int i=0;i<gb.size();i++) {
				groupby.add(gb.get(i).toString());
			}
		}
		
		Relation relation=null;
		
		
		
		
		//Execute From
		HeapFile f=c.getDbFile(c.getTableId(fromTable.getName()));
		ArrayList<Tuple> list=f.getAllTuples();		
		TupleDesc tuple=c.getTupleDesc(c.getTableId(fromTable.getName()));
		Relation from = new Relation(list,tuple);		
		relation=from;
		
		//Execute AS
		ArrayList<Integer> al=new ArrayList<Integer>();
		ArrayList<String> sl=new ArrayList<String>();
		for(int i=0;i<sItem.size();i++) {
			try {
				sl.add(((SelectExpressionItem)(sItem.get(i))).getAlias().getName());
				al.add(tuple.nameToId(sList.get(i).getColumn()));
				relation=relation.rename(al, sl);
			}
			catch(Exception e) {
				continue;
			}
		}
		
		//Execute Join
		if(!joinRight.isEmpty()) {
			for(int i=0;i<joinRight.size();i++) {
				int field1 = 0,field2 = 0;
				FromItem join = joinRight.get(i);
				Table joinTable=(Table)join;
				HeapFile file=c.getDbFile(c.getTableId(joinTable.getName()));
				TupleDesc td=c.getTupleDesc(c.getTableId(joinTable.getName()));
				Relation ar = new Relation(file.getAllTuples(),td);
				ArrayList<String> fields=new ArrayList<String>();
				StringTokenizer st =new StringTokenizer(joinOn.get(i),"=");
				while(st.hasMoreTokens()) {
					fields.add(st.nextToken());
				}
				for(int j=0;j<fields.size();j++) {
					StringTokenizer stk =new StringTokenizer(fields.get(j).trim(),".");
					String tableName = stk.nextToken();
					String colName = stk.nextToken();
					if(fromTable.getName().compareToIgnoreCase(tableName)==0) {
						field1 = relation.getDesc().nameToId(colName);
					}
					else {
						field2 = ar.getDesc().nameToId(colName);
					}
				}
				relation = relation.join(ar, field1, field2);
			}
		}
		
		//Execute Where
		if(wv.getLeft()!=null) {
			int sField = relation.getDesc().nameToId(wv.getLeft());
			Field whereOperand = wv.getRight();
			RelationalOperator whereOp=wv.getOp();
			relation=relation.select(sField, whereOp, whereOperand);
		}
		
		//Execute GroupBy
		ArrayList<Integer> groupBy= new ArrayList<Integer>();
		if(!groupby.isEmpty()) {
			for(int i=0;i<relation.getDesc().numFields();i++) {
				if(groupby.contains(relation.getDesc().getFieldName(i))) {
					groupBy.add(i);
				}	
			}	
			for(int i=0;i<relation.getDesc().numFields();i++) {
				if(!groupBy.contains(i))
					groupBy.add(i);
			}
			relation=relation.project(groupBy);
		}
	
		//Execute Aggregate
		Boolean GB=false;
		for(int i=0;i<sList.size();i++) {
			if(groupBy.isEmpty()) {
				if(sList.get(i).isAggregate()) {
					ArrayList<Integer> c = new ArrayList<Integer>();
					try {
						c.add(relation.getDesc().nameToId(sList.get(i).getColumn()));	
					}
					catch(Exception e){
						c.add(relation.getDesc().nameToId(((SelectExpressionItem)(sItem.get(i))).getAlias().getName()));
					}
					relation = relation.project(c);
					relation = relation.aggregate(sList.get(i).getOp(), GB);
				}
			}
			else {
				if(sList.get(i).isAggregate()) {
					relation = relation.aggregate(sList.get(i).getOp(),GB);
				}
				else {
					if(!groupBy.isEmpty())
						GB=true;
				}
			}
			
		}
		
		//Execute Select.
		ArrayList<Integer> projectList = new ArrayList<Integer>();
		for(int i=0;i<sList.size();i++) {
			if(sList.get(i).getColumn().equals("*")) {
				for(int j=0;j<relation.getDesc().numFields();j++) {
					projectList.add(j);
				}
				break;
			}
			else {
				try {
					projectList.add(relation.getDesc().nameToId(sList.get(i).getColumn()));
				}
				catch(Exception e){
					projectList.add(relation.getDesc().nameToId(((SelectExpressionItem)(sItem.get(i))).getAlias().getName()));
				}
			}
		}
		relation=relation.project(projectList);
		System.out.println(relation);
		return relation;
		
	}
}
