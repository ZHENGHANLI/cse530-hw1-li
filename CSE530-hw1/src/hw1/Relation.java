package hw1;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * This class provides methods to perform relational algebra operations. It will be used
 * to implement SQL queries.
 * @author Doug Shook
 *
 */
public class Relation {

	private ArrayList<Tuple> tuples;
	private TupleDesc td;
	
	public Relation(ArrayList<Tuple> l, TupleDesc td) {
		//your code here
		tuples=l;
		this.td=td;
	}
	
	/**
	 * This method performs a select operation on a relation
	 * @param field number (refer to TupleDesc) of the field to be compared, left side of comparison
	 * @param op the comparison operator
	 * @param operand a constant to be compared against the given column
	 * @return
	 */
	public Relation select(int field, RelationalOperator op, Field operand) {
		//your code here
		ArrayList<Tuple> list = new ArrayList<Tuple>();
			for(int i= 0;i<tuples.size();i++) {
				if(tuples.get(i).getField(field).compare(op, operand)) {
					list.add(tuples.get(i));
				}
			}
		Relation relation = new Relation(list,td);
		return relation;
	}
	
	/**
	 * This method performs a rename operation on a relation
	 * @param fields the field numbers (refer to TupleDesc) of the fields to be renamed
	 * @param names a list of new names. The order of these names is the same as the order of field numbers in the field list
	 * @return
	 */
	public Relation rename(ArrayList<Integer> fields, ArrayList<String> names) {
		//your code here
		if(fields.size()==names.size()) {
			Type[] typeAr = new Type[td.numFields()];
			String[] fieldAr=new String[td.numFields()];
			for(int i =0;i<td.numFields();i++) {
				typeAr[i]=td.getType(i);
				if(fields.contains(i)) {
					fieldAr[i]=names.get(fields.indexOf(i));
				}
				else {
					fieldAr[i]=td.getFieldName(i);
				}
			}
			TupleDesc tuple = new TupleDesc(typeAr,fieldAr);
			Relation relation = new Relation(tuples, tuple);
			return relation;
		}
		else 
			return this;
	}
	
	/**
	 * This method performs a project operation on a relation
	 * @param fields a list of field numbers (refer to TupleDesc) that should be in the result
	 * @return
	 */
	public Relation project(ArrayList<Integer> fields) {
		//your code here
		ArrayList<Type> typeArL = new ArrayList<Type>();
		ArrayList<String> fieldArL = new ArrayList<String>();
		
		for(int i =0;i<fields.size();i++) {
				typeArL.add(td.getType(fields.get(i)));
				fieldArL.add(td.getFieldName(fields.get(i)));
		}
		
		Type[]typeAr=new Type[typeArL.size()];
		String[]fieldAr=new String[fieldArL.size()];
		for(int i=0;i<typeArL.size();i++) {
			typeAr[i]=typeArL.get(i);
			fieldAr[i]=fieldArL.get(i);
		}
		TupleDesc tupled = new TupleDesc(typeAr,fieldAr);
		ArrayList<Tuple> list=new ArrayList<Tuple>();
		for(int i= 0;i<tuples.size();i++) {
			Tuple tuple = new Tuple(tupled);
			for(int j=0;j<fields.size();j++) {
				tuple.setField(j, tuples.get(i).getField(fields.get(j)));
			}
			list.add(tuple);
		}
		Relation relation = new Relation(list,tupled);
		return relation;
	}
	
	/**
	 * This method performs a join between this relation and a second relation.
	 * The resulting relation will contain all of the columns from both of the given relations,
	 * joined using the equality operator (=)
	 * @param other the relation to be joined
	 * @param field1 the field number (refer to TupleDesc) from this relation to be used in the join condition
	 * @param field2 the field number (refer to TupleDesc) from other to be used in the join condition
	 * @return
	 */
	public Relation join(Relation other, int field1, int field2) {
		//your code here
		//select * from t1 inner join t2 on t1.c1=t2.a1;
		ArrayList<Type> typeArL=new ArrayList<Type>();
		ArrayList<String> fieldArL=new ArrayList<String>();
		for(int i=0;i<td.numFields();i++) {
			typeArL.add(td.getType(i));
			fieldArL.add(td.getFieldName(i));
		}
		for(int i=0;i<other.getDesc().numFields();i++) {
			typeArL.add(other.getDesc().getType(i));
			fieldArL.add(other.getDesc().getFieldName(i));
		}
		Type[] typeAr = new Type[typeArL.size()];
		String[]fieldAr=new String[fieldArL.size()];
		for(int i=0;i<typeArL.size();i++) {
			typeAr[i]=typeArL.get(i);
			fieldAr[i]=fieldArL.get(i);
		}

		ArrayList<Tuple> list = new ArrayList<Tuple>();
		TupleDesc tupled = new TupleDesc(typeAr,fieldAr);

		for(int i=0;i<tuples.size();i++) {
			for(int j=0;j<other.getTuples().size();j++) {
				Field field=other.getTuples().get(j).getField(field2);			
				if(tuples.get(i).getField(field1).compare(RelationalOperator.EQ,field)) {
					//list.add(tuples.get(i));
					Tuple temp = new Tuple(tupled);
					for(int k=0;k<td.numFields();k++) {
						temp.setField(k, tuples.get(i).getField(k));
					}
					for(int k=td.numFields();k<tupled.numFields();k++) {
						temp.setField(k, other.getTuples().get(j).getField(k-td.numFields()));
					}
					list.add(temp);
				}
			}
		}
		
		
		Relation relation = new Relation(list,tupled);
		return relation;
	}
	
	/**
	 * Performs an aggregation operation on a relation. See the lab write up for details.
	 * @param op the aggregation operation to be performed
	 * @param groupBy whether or not a grouping should be performed
	 * @return
	 */
	public Relation aggregate(AggregateOperator op, boolean groupBy) {
		//your code here
		Aggregator agg= new Aggregator(op,groupBy,td);
		for(int i=0;i<tuples.size();i++) {
			agg.merge(tuples.get(i));
		}
		Relation relation=new Relation(agg.getResults(),td);
		//System.out.println(agg.getResults().get(0));
		return relation;
	}
	
	public TupleDesc getDesc() {
		//your code here
		return td;
	}
	
	public ArrayList<Tuple> getTuples() {
		//your code here
		return tuples;
	}
	
	/**
	 * Returns a string representation of this relation. The string representation should
	 * first contain the TupleDesc, followed by each of the tuples in this relation
	 */
	public String toString() {
		//your code here
		String s="";
		for(int i=0;i<td.numFields();i++) {
			String temp = td.getType(i)+"("+td.getFieldName(i)+")";
			s+=(temp+"\t");
		}
		s+="\n";
		for(int i=0;i<tuples.size();i++) {
			for(int j=0;j<td.numFields();j++) {
				s+=tuples.get(i).getField(j);
				s+=" \t";
			}
			s+="\n";
		}
		return s;
	}
}
