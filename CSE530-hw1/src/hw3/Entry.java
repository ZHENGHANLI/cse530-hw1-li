package hw3;

import hw1.Field;
import hw1.RelationalOperator;
import hw1.Tuple;
public class Entry {

	private Tuple tuple;
	private int page;
	
	public Entry(Tuple tuple, int page) {
		this.tuple = tuple;
		this.page = page;
	}
	
	public Tuple getTuple() {
		return this.tuple;
	}
	
	public int getPage() {
		return this.page;
	}
	
	public boolean equals(Object obj) {
		Entry e = (Entry)obj;	
		if((tuple.getId()==e.getTuple().getId())&&(tuple.getPid()==e.getTuple().getPid())) {
			return true;
		}
		else {
			return false;
		}	
	}
}
