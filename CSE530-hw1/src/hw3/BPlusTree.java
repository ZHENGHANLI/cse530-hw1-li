package hw3;


import hw1.Field;
import hw1.RelationalOperator;

public class BPlusTree {
    private int degree;
    private Node root;
    public BPlusTree(int degree) {
    	//your code here
    		this.degree=degree;
    		root=new LeafNode(degree);
    }
    public LeafNode search(Field f) {
    	//your code here
    		return searchRecur(root,f);
    }
    //return null if not exist in B+ Tree
    private LeafNode searchRecur(Node root, Field f) {
    		if(root.isLeafNode()) {
    			LeafNode node=(LeafNode)root;
			for(int i=0;i<node.getEntries().size();i++) {
				if(node.getEntries().get(i).getField().compare(RelationalOperator.EQ, f)) {
					return (LeafNode) root;
				}
			}
		}
		else {
			InnerNode node=(InnerNode)root;
			for(int i=0;i<node.getKeys().size();i++) {
				if(node.getKeys().get(i).compare(RelationalOperator.GTE, f)) {
					return searchRecur(node.getChildren().get(i),f);
				}
			}
			return searchRecur(node.getChildren().get(node.getKeys().size()),f);
		}
    		return null;
    }
    
    //Search the Node for insertion, always return a LeafNode 
    private LeafNode searchNode(Node root, Entry e) {
    		if(root.isLeafNode()) {
    			return (LeafNode) root;
    		}
    		else {
    			InnerNode node=(InnerNode)root;
    			for(int i=0;i<node.getKeys().size();i++) {
    				if(node.getKeys().get(i).compare(RelationalOperator.GTE, e.getField())) {
    					return searchNode(node.getChildren().get(i),e);
    				}
    			}
    			return searchNode(node.getChildren().get(node.getKeys().size()),e);
    		}
    }
    public void insert(Entry e) {
    		//your code here 	
    		if(search(e.getField())==null) {
			//Search for the node 
    			LeafNode nodeInsert = searchNode(root,e);    			
    			//Insert if it is not full
    			if(nodeInsert.getEntries().size()<degree) {
    				nodeInsert.insert(e);
    			}
    			else {
    				nodeInsert.insert(e);
    				makeNewNode(nodeInsert);	
    				split(nodeInsert.getParent());
    			}
    			
    		}		
    }
    private void makeNewNode(LeafNode node) {
    		InnerNode parent = node.getParent();
    		LeafNode newNode = new LeafNode(degree);
    		double middle = node.getEntries().size()/2.0;
    		for(int i=0;i<middle;i++) {
    			newNode.insert(node.getEntries().get(0));
    			node.remove(node.getEntries().get(0));
    		}
    		if(parent==null) {
    			InnerNode newRoot = new InnerNode(degree);
    			newRoot.insert(newNode);
    			newNode.setParent(newRoot);
    			node.setParent(newRoot);
    			newRoot.insert(node);
    			newRoot.insertKey(newNode.getEntries().get(newNode.getEntries().size()-1).getField());
    			root=newRoot;
    		}
    		else {
    			newNode.setParent(parent);
    			parent.insert(newNode);
    			parent.insertKey(newNode.getEntries().get(newNode.getEntries().size()-1).getField());
    		}
    }
    private void split(InnerNode node) {
    		if(node.getKeys().size()>node.getDegree()) {
    			InnerNode parent = node.getParent();	
    			InnerNode Left = new InnerNode(degree);
    			InnerNode Right = new InnerNode(degree);
    			double middle = node.getKeys().size()/2.0;
    			int mid;
    			if((node.getKeys().size()%2)!=0) {
    				mid = (int)middle;
    			}
    			else {
    				mid = ((int)(middle))-1;
    			}
    			for(int i=0;i<mid;i++) {
    				Left.insertKey(node.getKeys().get(i));
    			}
    			for(int i=mid+1;i<node.getKeys().size();i++) {
    				Right.insertKey(node.getKeys().get(i));
    			}
    			for(int i=0;i<Left.getKeys().size()+1;i++) {
    				Left.insert(node.getChildren().get(i));
    				node.getChildren().get(i).setParent(Left);
    			}
    			for(int i=Left.getKeys().size()+1;i<node.getChildren().size();i++) {
    				Right.insert(node.getChildren().get(i));
    				node.getChildren().get(i).setParent(Right);
    			}
    			if(parent==null) {
    				InnerNode newRoot = new InnerNode(degree);
    				newRoot.insertKey(node.getKeys().get(mid));
    				Left.setParent(newRoot);
    				Right.setParent(newRoot);
    				newRoot.insert(Left);
    				newRoot.insert(Right);
    				root=newRoot;
    			}
    			else {
    				parent.insertKey(node.getKeys().get(mid));  				
    				parent.remove(node);
    				parent.insert(Left);
    				Left.setParent(parent);
    				Right.setParent(parent);
    				parent.insert(Right);		
    				split(parent);
    			}
    			
    		}
    }
    public void delete(Entry e) {
    	//your code here
    		//Search the node delete
    		LeafNode nodeDelete = search(e.getField());
    		if(nodeDelete!=null) {
    			nodeDelete.remove(e);
    			if(nodeDelete.getEntries().size() < (nodeDelete.getDegree()/2)) {
    				InnerNode parent = nodeDelete.getParent();
    				//Get all siblings
    				if(parent!=null) {
    					Boolean canBorrow = false;
    					for(int i=0;i<parent.getChildren().size();i++) {
    						//Cast to LeafNode because all siblings are LeafNode
    						LeafNode temp = (LeafNode)(parent.getChildren().get(i));
    						if(temp.getEntries().size()>(temp.getDegree()/2)) {
    							canBorrow = true;
    						}
    					}
    					if(canBorrow) {
    						Borrow(nodeDelete);
    					}
    					else {
    						Merge(nodeDelete);
    					}
    				}
    			}
    			else {
    				//Replace delete value by new value
    				Field valueToPush;
    				if(e.getField().compare(RelationalOperator.GT, nodeDelete.getEntries().get(nodeDelete.getEntries().size()-1).getField())) {
    					valueToPush = nodeDelete.getEntries().get(nodeDelete.getEntries().size()-1).getField();
    				}
    				else {
    					valueToPush = nodeDelete.getEntries().get(0).getField();
    				}
    				InnerNode temp=nodeDelete.getParent();
    				while(temp!=null) {
    					for(int i =0;i<temp.getKeys().size();i++) {
    						if(temp.getKeys().get(i).compare(RelationalOperator.EQ, e.getField())) {
        						temp.removeKey(e.getField());
        						temp.insertKey(valueToPush);
    						}
    					}
    					temp=temp.getParent();	
    				}
    			}
    		}
    }

    private void Borrow(LeafNode node) {
    		//parent can not be null, no more check needed
    		InnerNode parent = node.getParent();
    		int position = parent.getChildren().indexOf(node);
    		for(int i=0;i<parent.getChildren().size();i++) {
    			if(i!=position) {
    				LeafNode temp = (LeafNode)(parent.getChildren().get(i));
        			if(temp.getEntries().size()>=(temp.getDegree()/2)) {
        				if(i<position) {
        					Entry remove = temp.getEntries().get(temp.getEntries().size()-1);
        					node.insert(temp.remove(remove));
        				}
        				else {
        					Entry remove = temp.getEntries().get(0);
        					node.insert(temp.remove(remove));
        				}
        				break;
        			}
    			}	
    			else {
    				continue;
    			}
    		}	
    		//update parent
    		parent.getKeys().clear();
    		for(int i=0;i<parent.getChildren().size()-1;i++) {
    			LeafNode temp = (LeafNode)(parent.getChildren().get(i));
    			parent.insertKey(temp.getEntries().get(temp.getEntries().size()-1).getField());
    		}
    }
      
    private void Merge(Node node) {
    		//Merge node with siblings
    		Grab(node);
    		InnerNode parent = node.getParent();
    		pushThrough(parent);
    }
    private void Grab(Node node) {
    		InnerNode parent = node.getParent();
		int position = parent.getChildren().indexOf(node);
		Node temp;
		if(position==0) {
			temp = (parent.getChildren().get(position+1));
		}
		else {   			
    			temp = (parent.getChildren().get(position-1));  		
		}
		//grab node from other sibling
		if(node.isLeafNode()) {
			LeafNode lnode = (LeafNode)node;
			parent.remove(node);
			for(int i=0;i<lnode.getEntries().size();i++) {
				((LeafNode)temp).insert(lnode.getEntries().get(i));
			}
			//Delete entry from parent
			System.out.println();
			if(position>=parent.getChildren().size()) {
				parent.removeKey(parent.getKeys().get(position-1));
			}
			else {
		
				parent.removeKey(parent.getKeys().get(position));
			}
		}
		else {
			InnerNode inode = (InnerNode)node;
			parent.remove(node);
			for(int i=0;i<inode.getKeys().size();i++) {
				((InnerNode)temp).insertKey(inode.getKeys().get(i));
			}
			for(int i=0;i<inode.getChildren().size();i++) {
				((InnerNode)temp).insert(inode.getChildren().get(i));
			}
			//Grab value from parent if there is not enough keys
			if(((InnerNode)temp).getChildren().size()-1>((InnerNode)temp).getKeys().size()) {
				((InnerNode)temp).insertKey(parent.getKeys().get(position-1));
				parent.removeKey((parent.getKeys().get(position-1)));
			}
			else {
				((InnerNode)temp).insertKey(parent.getKeys().get(position));
				parent.removeKey(parent.getKeys().get(position));
			}
			split((InnerNode)temp);
		}
		node = temp;
    }

    private void pushThrough(InnerNode node) {
    		if(node.getParent()!=null) {
    			if(node.getKeys().size()<node.getDegree()/2) {
        			Merge(node);
        		}
    		}
    		else if((node.getKeys().size()==0)&&(node.getChildren().size()==1)){
    			node=(InnerNode) node.getChildren().get(0);
    			root=node;
    			root.setParent(null);
    		}
    		
    }
    public Node getRoot() {
    	//your code here
    		return root;
    }	
}
