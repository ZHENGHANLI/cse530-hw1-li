package hw3;

import java.util.ArrayList;
import java.util.Collections;

import hw1.RelationalOperator;

import java.util.*;

public class LeafNode implements Node {
	private int degree;
	private ArrayList<Entry> list;
	private InnerNode parent;
	
	
	public LeafNode(int degree) {
		//your code here
		this.degree=degree;
		list = new ArrayList<Entry>();
		parent=null;
	}
	
	public void insert(Entry e) {
		list.add(e);
		Collections.sort(list, new Comparator<Entry>() {
			@Override
			public int compare(Entry o1, Entry o2) {
				if(o1.getField().compare(RelationalOperator.GT, o2.getField()))
					return 1;
				else if(o1.getField().compare(RelationalOperator.LT, o2.getField()))
					return -1;
				else
					return 0;
			}
		});
	}
	
	public void setParent(InnerNode node) {
		parent = node;
	}
	public InnerNode getParent() {
		return parent;
	}
	
	public ArrayList<Entry> getEntries() {
		//your code here
		return list;
	}

	public int getDegree() {
		//your code here
		return degree;
	}
	
	public Entry remove(Entry e) {
		this.list.remove(e);
		return e;
	}
	
	public boolean isLeafNode() {
		return true;
	}
	
	public boolean equals(Object obj) {
		LeafNode node = (LeafNode)obj;
		if(node.list.equals(list)) {
			return true;
		}
		else {
			return false;
		}
	}
}