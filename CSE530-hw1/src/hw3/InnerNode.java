package hw3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import hw1.Field;
import hw1.RelationalOperator;

public class InnerNode implements Node {
	private int degree;
	private ArrayList<Field> keys;
	private ArrayList<Node> children;
	private InnerNode parent;
	public InnerNode(int degree) {
		//your code here
		this.degree=degree;
		keys=new ArrayList<Field>();
		children=new ArrayList<Node>();
		parent=null;
	}
	public void setParent(InnerNode node) {
		parent=node;
	}
	public InnerNode getParent() {
		return parent;
	}
	public ArrayList<Field> getKeys() {
		//your code here
		return keys;
	}
	//insert key
	public void insertKey(Field f) {
		keys.add(f);
		Collections.sort(keys, new Comparator<Field>() {
			@Override
			public int compare(Field o1, Field o2) {
				if(o1.compare(RelationalOperator.GT, o2))
					return 1;
				else if(o1.compare(RelationalOperator.LT, o2))
					return -1;
				else
					return 0;
			}
		});
	}
	
	//insert children
	public void insert(Node node) {
		children.add(node);
		Collections.sort(children, new Comparator<Node>() {
			@Override
			public int compare(Node o1, Node o2) {
				if(o1.isLeafNode()) {				
					if(((LeafNode)o1).getEntries().get(0).getField().compare(RelationalOperator.GT, ((LeafNode)o2).getEntries().get(0).getField()))
						return 1;
					else if(((LeafNode)o1).getEntries().get(0).getField().compare(RelationalOperator.LT, ((LeafNode)o2).getEntries().get(0).getField()))
						return -1;
					else
						return 0;
				}
				else {
						if(((InnerNode)o1).getKeys().get(0).compare(RelationalOperator.GT, ((InnerNode)o2).getKeys().get(0)))
							return 1;
						else if(((InnerNode)o1).getKeys().get(0).compare(RelationalOperator.LT, ((InnerNode)o2).getKeys().get(0)))
							return -1;
						else
							return 0;
				}
			}
			
		});
	}
	
	public Node remove(Node node) {
		children.remove(node);
		return node;
	}
	
	public Field removeKey(Field f) {
		keys.remove(f);
		return f;
	}
	
	public ArrayList<Node> getChildren() {
		//your code here
		return children;
	}

	public int getDegree() {
		//your code here
		return degree;
	}
	
	public boolean isLeafNode() {
		return false;
	}
	
	public boolean equals(Object obj) {
		InnerNode node = (InnerNode)obj;
		if(node.keys.equals(keys)&&node.children.equals(children)) {
			return true;
		}
		else {
			return false;
		}
	}


}